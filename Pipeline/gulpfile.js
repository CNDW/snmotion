const gulp = require('gulp');
const notifier = require('node-notifier');

const SOURCE = [
    '../mod.json',
    '../bin/Debug/SNMotion.dll',
    '../bin/Debug/Valve.Newtonsoft.Json.dll',
    '../bin/Debug/websocket-sharp.dll',
    '../bin/Debug/CNDW.Harmony.dll',
    '../bin/Debug/System.Runtime.Serialization.dll',
    '../Rig/Assets/StreamingAssets/snmotion',
    '../Rig/Assets/StreamingAssets/snmotion.manifest',
    '../actions.json',
    '../bindings_holographic_controller.json',
    '../bindings_knuckles.json',
    '../bindings_oculus_touch.json',
    '../bindings_vive_controller.json',
];

gulp.task('default', ['copy', 'watch']);

gulp.task('copy', () => {
    gulp.src(SOURCE)
        .pipe(gulp.dest('/mnt/d/steam/steamapps/common/Subnautica/QMods/SNMotion/'))
        .on('end', () => {
            notifier.notify({
                title: 'SNMotion Pipeline',
                message: 'SNMotion mod files copied to QMods folder',
                sound: false
            });
        });
});

gulp.task('watch', () => {
    gulp.watch(SOURCE, ['copy']);
});