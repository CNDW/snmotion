﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Reflection;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using WebSocketSharp;
using Valve.Newtonsoft.Json.Linq;

namespace ModInspector {
    public class InspectorActionHandler : ScriptableObject {
        private static void QueueHandler(IEnumerator handler) {
            UnityMainThreadDispatcher.Instance().Enqueue(handler);
        }

        public static void OnMessage(object sender, MessageEventArgs e) {
            JArray message = JArray.Parse(e.Data);
            string command = (message[0] as JValue).ToString();
            JObject data = message[1] as JObject;

            switch (command) {
                case "getGameObject":
                    QueueHandler(GetGameObject(data.GetValue("path").ToString()));
                    break;
                case "getScenes":
                    QueueHandler(GetScenes());
                    break;
                case "getScene":
                    QueueHandler(GetScene(data.GetValue("path").ToString()));
                    break;
                case "setValues":
                    QueueHandler(SetValues(data));
                    break;
                default:
                    Debug.Log("Unhandled websocket command " + command);
                    break;
            }

        }

        public static void SetProperty(string compoundProperty, object target, object value) {
            string[] bits = compoundProperty.Split('.');
            for (int i = 0; i < bits.Length - 1; i++) {
                PropertyInfo propertyToGet = target.GetType().GetProperty(bits[i]);
                target = propertyToGet.GetValue(target, null);
            }
            PropertyInfo propertyToSet = target.GetType().GetProperty(bits[bits.Length - 1]);
            propertyToSet.SetValue(target, value, null);
        }


        public static void SetValues(JObject action, GameObject obj) {
            object target = action["componentName"] == null ? obj : (object)obj.GetComponent((string)action["componentName"]);
            if (target == null) {
                Inspector.Send("unresolvedTargetError", action);
                return;
            }
            switch ((string)action["targetType"]) {
                case "UnityEngine.Vector3":
                    foreach (JProperty p in action["values"]) {
                        Vector3 vector = new Vector3(
                            float.Parse((string)p.Value["x"]),
                            float.Parse((string)p.Value["y"]),
                            float.Parse((string)p.Value["z"])
                        );
                        SetProperty(p.Name, target, vector);
                    }
                    break;
                case "UnityEngine.Vector2":
                    foreach (JProperty p in action["values"]) {
                        Vector2 vector = new Vector2(
                            float.Parse((string)p.Value["x"]),
                            float.Parse((string)p.Value["y"])
                        );
                        SetProperty(p.Name, target, vector);
                    }
                    break;
                default:
                    Inspector.Send("unhandledSetValues", action);
                    break;
            }
        }

        public static IEnumerator SetValues(JObject action) {
            string path = (string)action["objectPath"];

            GameObject obj = GameObject.Find(path);

            if (obj == null) {
                Inspector.Send("missingGameObject", new JObject(new JProperty("path", path)));
                yield return null;
            } else {
                SetValues(action, obj);

                yield return GetGameObject(path);
            }
        }

        public static IEnumerator GetGameObject(string path) {
            GameObject obj = GameObject.Find(path);

            if (obj == null) {
                Inspector.Send("missingGameObject", new JObject(new JProperty("path", path)));
            } else {
                Inspector.Send("gameObject", InspectorSerializer.TreeToJson(obj, "", path));
                try {
					MonoBehaviour[] components = obj.GetComponents<MonoBehaviour>();
                    Inspector.Send("components", InspectorSerializer.ToJson(components, path));
                } catch (Exception err) {
                    Debug.Log("Failed to serialize components for gameObject: " + obj.name);
                    Debug.Log(err.ToString());
                }
				Camera camera = obj.GetComponent<Camera> ();
				if (camera != null) {
					Inspector.Send("components", InspectorSerializer.ToJson(camera, path));
				}

				Image image = obj.GetComponent<Image> ();
				if (image != null) {
					Inspector.Send ("components", InspectorSerializer.ToJson (image, path));
				}

				Graphic graphic = obj.GetComponent<Graphic> ();
				if (graphic != null) {
					Inspector.Send ("components", InspectorSerializer.ToJson (graphic, path));
				}

                BoxCollider collider = obj.GetComponent<BoxCollider>();
                if (collider != null) {
                    Inspector.Send("components", InspectorSerializer.ToJson(collider, path));
                }

                RectTransform t = obj.GetComponent<RectTransform>();
                if (t != null) {
                    Inspector.Send("transform", InspectorSerializer.ToJson(t, path));
                } else {
                    Inspector.Send("transform", InspectorSerializer.ToJson(obj.transform, path));
                }
            }

            yield return null;
        }

        public static IEnumerator GetScene(string path) {
            Debug.Log("Searching for Scene at: " + path);
            Scene scene = path == "DontDestroyOnLoad" ? 
                GetDontDestroyOnLoadScene() :
                SceneManager.GetSceneByName(path);

            foreach (GameObject root in scene.GetRootGameObjects()) {
                Inspector.Send("gameObject", InspectorSerializer.TreeToJson(root, ""));
            }
            Inspector.Send("scene", InspectorSerializer.ToJson(scene, true));

            yield return null;
        }

        private static Scene GetDontDestroyOnLoadScene() {
            // Majestic hack for retrieving the hidden `dontDestroyOnLoad` scene
            GameObject temp = null;
            try {
                temp = new GameObject();
                UnityEngine.Object.DontDestroyOnLoad(temp);
                UnityEngine.SceneManagement.Scene dontDestroyOnLoad = temp.scene;
                UnityEngine.Object.DestroyImmediate(temp);
                temp = null;

                return dontDestroyOnLoad;
            } finally {
                if (temp != null)
                    UnityEngine.Object.DestroyImmediate(temp);
            }
        }

        public static IEnumerator GetScenes() {
            int count = SceneManager.sceneCount;
            while (count > 0) {
                Scene scene = SceneManager.GetSceneAt(count - 1);
                Inspector.Send("scene", InspectorSerializer.ToJson(scene));
                count--;
            }

            Scene dontDestroy = GetDontDestroyOnLoadScene();
            if (dontDestroy != null) {
                Inspector.Send("scene", InspectorSerializer.ToJson(dontDestroy));
            }

            yield return null;
        }
    }
}
