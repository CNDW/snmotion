﻿using System;
using Valve.Newtonsoft.Json.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace ModInspector
{
	public class InspectorSerializer
	{
        public static JObject ToJson(Scene scene, bool includeChildren = false) {
            return new SceneResource(scene, includeChildren).ToJson();
		}

		public static JObject ToJson(Transform t, string parentPath) {
            return new TransformResource(t, parentPath).ToJson();
		}

        public static JObject ToJson(RectTransform t, string parentPath) {
            return new RectTransformResource((Transform)t, parentPath).ToJson();
		}

        public static JObject ToJson(GameObject obj, string parentPath = "", string objPath = null) {
            return new GameObjectResource(obj, parentPath, objPath).ToJson();
        }

		public static JObject ToJson(Camera obj, string parentPath) {
			return new CameraResource (obj, parentPath).ToJson();
		}

		public static JObject ToJson(Image obj, string parentPath) {
			return new ImageResource (obj, parentPath).ToJson();
		}

		public static JObject ToJson(Graphic obj, string parentPath) {
			return new GraphicResource (obj, parentPath).ToJson();
		}
        public static JObject ToJson(BoxCollider obj, string parentPath) {
			return new BoxColliderResource (obj, parentPath).ToJson();
		}

		public static JArray TreeToJson(GameObject obj, string parentPath = "", string objPath = null, JArray objects = null) {
            if (objects == null) {
                objects = new JArray();
            }

            GameObjectResource resource = new GameObjectResource(obj, parentPath, objPath);
            objects.Add(resource.ToJson());

            foreach (Transform child in obj.transform) {
                TreeToJson(child.gameObject, resource.id, null, objects);
            }

            return objects;
		}

        public static JArray ToJson(MonoBehaviour[] components, string parentPath) {
            JArray payload = new JArray();

            foreach (MonoBehaviour component in components) {
                payload.Add(ToJson(component, parentPath));
            }

            return payload;
        }

        public static JObject ToJson(MonoBehaviour component, string parentPath, string parentComponent = "") {
            return new ComponentResource(component, parentPath).ToJson();
        }
	}
}

