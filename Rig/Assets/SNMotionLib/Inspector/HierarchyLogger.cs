﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace ModInspector {

	public class HierarchyLogger
	{
        public static void Initialize()
        {
            Debug.Log("HierarchyLogger initializing...");
            SceneManager.sceneLoaded += OnSceneLoaded; 
        }

		public static void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
            foreach (GameObject root in scene.GetRootGameObjects()) {
                Inspector.Send("gameObject", InspectorSerializer.TreeToJson(root, ""));
            }
            Inspector.Send("scene", InspectorSerializer.ToJson(scene));
        }

    }
}

