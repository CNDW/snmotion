﻿using UnityEngine;
using Valve.Newtonsoft.Json;
using Valve.Newtonsoft.Json.Linq;

namespace ModInspector {
    public class RectTransformResource : TransformResource {
        protected RectTransform rt;

        public RectTransformResource(Transform transform, string _parentPath) : base( transform, _parentPath ) {
            rt = (RectTransform)transform;
            parentPath = _parentPath;
			id = _parentPath + "@@RectTransform";
            type = "rect-transform";
        }

        [JsonProperty("anchored-position")]
        public JObject AnchoredPosition { get { return VectorToJson(rt.anchoredPosition); } }
        [JsonProperty("anchored-position3-d")]
        public JObject AnchoredPosition3D { get { return VectorToJson(rt.anchoredPosition3D); } }
        [JsonProperty("anchor-max")]
        public JObject AnchorMax { get { return VectorToJson(rt.anchorMax); } }
        [JsonProperty("anchor-min")]
        public JObject AnchorMin { get { return VectorToJson(rt.anchorMin); } }
        [JsonProperty("offset-max")]
        public JObject OffsetMax { get { return VectorToJson(rt.offsetMax); } }
        [JsonProperty("offset-min")]
        public JObject OffsetMin { get { return VectorToJson(rt.offsetMin); } }
        [JsonProperty("pivot")]
        public JObject Pivot { get { return VectorToJson(rt.pivot); } }
        [JsonProperty("rect")]
        public JObject Rect { get { return RectToJson(rt.rect); } }
        [JsonProperty("size-delta")]
        public JObject SizeDelta { get { return VectorToJson(rt.sizeDelta); } }

    }
}
