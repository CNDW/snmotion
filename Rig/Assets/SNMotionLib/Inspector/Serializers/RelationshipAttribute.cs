﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModInspector {
    class RelationshipAttribute : Attribute {
        public string PropertyName { get; set; }

        public RelationshipAttribute(string propertyName) {
            PropertyName = propertyName;
        }
    }
}
