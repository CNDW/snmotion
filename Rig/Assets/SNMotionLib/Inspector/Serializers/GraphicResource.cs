﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Reflection;
using Valve.Newtonsoft.Json;
using Valve.Newtonsoft.Json.Linq;

namespace ModInspector {
	class GraphicResource : JsonResource {

		protected Graphic component;
		protected string parentPath;

		public GraphicResource(Graphic _component, string _parentPath) {
			id = _parentPath + "@@Graphic";
			type = "game-component";
			component = _component;
			parentPath = _parentPath;
		}

		[JsonProperty("unity-type")]
		public string UnityType { get { return component.GetType().ToString(); } }

		[JsonProperty("name")]
		public string Name { get { return "Graphic"; } }

		[JsonProperty("fields")]
		public JObject Fields {
			get {
				PropertyInfo[] fields = component.GetType().GetProperties(flags);
				JObject attributes = new JObject();
				foreach (PropertyInfo field in fields) {
					try {
						if (field.PropertyType.BaseType == typeof(MonoBehaviour)) {
							MonoBehaviour mono = (MonoBehaviour)field.GetValue(component, null);
							if (mono != null) {
								JObject value = new JObject(
									new JProperty("value", mono.ToString()),
									new JProperty("type", field.PropertyType.BaseType.ToString()),
									new JProperty("isWritable", "False")
								);
							}
						} else {
							var fieldValue = field.GetValue(component, null);

							if (fieldValue == null) {
								fieldValue = field.PropertyType;
							}

							JObject value = new JObject(

								new JProperty("value", fieldValue.ToString()),
								new JProperty("type", field.PropertyType.BaseType.ToString()),
								new JProperty("isWritable", "True")
							);
							attributes.Add(new JProperty(field.Name, value));
						}
					} catch (Exception err) {
						Debug.Log ("Failed to serialize property '" + field.Name + "'");
						Debug.Log(err.ToString());
					}
				}
				return attributes;
			}
		}

		[Relationship("game-object")]
		public JObject GameObject { get { return ToRelationship(parentPath, "game-object"); } }
	}
}

