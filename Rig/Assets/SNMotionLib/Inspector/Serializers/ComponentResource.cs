﻿using UnityEngine;
using System;
using System.Reflection;
using Valve.Newtonsoft.Json;
using Valve.Newtonsoft.Json.Linq;

namespace ModInspector {
    class ComponentResource : JsonResource {
        protected MonoBehaviour component;
        protected Type componentType;
        protected string parentPath;

        public ComponentResource(MonoBehaviour _component, string _parentPath) {
            componentType = _component.GetType();
            id = _parentPath + "@@" + componentType.ToString();
            type = "game-component";
			component = _component;
			parentPath = _parentPath;
        }

        [JsonProperty("unity-type")]
        public string UnityType { get { return componentType.ToString(); } }

        [JsonProperty("name")]
        public string Name { get { return componentType.ToString(); } }

        [JsonProperty("fields")]
        public JObject Fields { get {
            FieldInfo[] fields = componentType.GetFields(flags);
            JObject attributes = new JObject();
            foreach (FieldInfo field in fields) {
                try {
                    if (field.FieldType.BaseType == typeof(MonoBehaviour)) {
                            MonoBehaviour mono = (MonoBehaviour)field.GetValue(component);
                            if (mono != null) {
                                JObject value = new JObject(
                                    new JProperty("value", mono.ToString()),
                                    new JProperty("type", field.FieldType.BaseType.ToString()),
                                    new JProperty("isWritable", "False")
                                );
                            } else {
                                JObject value = new JObject(
                                    new JProperty("value", field.GetValue(component).ToString()),
                                    new JProperty("type", field.FieldType.BaseType.ToString()),
                                    new JProperty("isWritable", "False")
                                );
                            }

                    } else {
                        var fieldValue = field.GetValue(component);

                        if (fieldValue == null) {
                            fieldValue = field.FieldType;
                        }

                        JObject value = new JObject(
                            new JProperty("value", fieldValue.ToString()),
                            new JProperty("type", field.FieldType.BaseType.ToString()),
                            new JProperty("isWritable", "True")
                        );
                        attributes.Add(new JProperty(field.Name, value));
                    }
                } catch {
                    attributes.Add(new JProperty(field.Name,  new JObject(
                        new JProperty("value", "Unserialializable value"),
                        new JProperty("type", "Unknown type"),
                        new JProperty("isWritable", "False")
                    )));
                }
            }
            return attributes;
        } }

        [Relationship("game-object")]
		public JObject GameObject { get { return ToRelationship(parentPath, "game-object"); } }
    }
}
