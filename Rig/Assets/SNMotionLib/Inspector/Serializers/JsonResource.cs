﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Valve.Newtonsoft.Json.Linq;
using Valve.Newtonsoft.Json;


namespace ModInspector {
    public class JsonResource {
		protected static BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic;
        public string id;
        public string type;
        protected JObject relationships {
            get {
                JObject _relationships = new JObject();

                foreach (PropertyInfo p in GetType().GetProperties()) {
                    foreach (Attribute a in p.GetCustomAttributes(true)) {
                        if (a.GetType() == typeof(RelationshipAttribute)) {
                            RelationshipAttribute rel = (RelationshipAttribute)a;
                            var relationship = p.GetValue(this, null);
                            if (relationship != null) {
                                _relationships.Add(
                                    new JProperty(rel.PropertyName, new JObject(
                                        new JProperty("data", relationship)
                                    ))
                                );
                            }
                        }
                    }
                }

                return _relationships;
            }
        }
        protected JObject attributes {
            get {
                JObject _attributes = new JObject();

                foreach (PropertyInfo p in GetType().GetProperties()) {
                    foreach (Attribute a in p.GetCustomAttributes(true)) {
                        if (a.GetType() == typeof(JsonPropertyAttribute)) {
                            JsonPropertyAttribute attr = (JsonPropertyAttribute)a;
                            _attributes.Add(new JProperty(attr.PropertyName, p.GetValue(this, null)));
                        }
                    }
                }

                return _attributes;
            }
        }

        public void AddRelationship(string attrName, string foreignType, string foreignId) {
            relationships.Add(
                new JProperty(attrName, new JObject(
                    new JProperty("data", new JObject(
                        new JProperty("id", foreignId),
                        new JProperty("type", foreignType)
                    ))
                ))
            );

        }

        public JObject ToJson() {
            if (id == null || type == null) {
                throw new Exception("JsonResource '{0}' missing required id or type members");
            }

            JObject json = new JObject(
                new JProperty("id", id),
                new JProperty("type", type),
                new JProperty("attributes", attributes)
            );

            if (relationships.Count > 0) {
                json.Add("relationships", relationships);
            }

            return json;
        }

        public JObject ToRelationship(string id, string type) {
            return new JObject(
                new JProperty("id", id),
                new JProperty("type", type)
            );
        }

        public override string ToString() {
            return ToJson().ToString();
        }
        public JObject VectorToJson(Vector3 v) {
            return new JObject(
                new JProperty("unity-type", typeof(Vector3).ToString()),
                new JProperty("magnitude", v.magnitude),
                new JProperty("sqr-magnitude", v.sqrMagnitude),
                new JProperty("x", v.x),
                new JProperty("y", v.y),
                new JProperty("z", v.z),
                new JProperty("value", v.ToString())
            );
        }

        public JObject VectorToJson(Vector2 v) {
            return new JObject(
                new JProperty("unity-type", typeof(Vector2).ToString()),
                new JProperty("magnitude", v.magnitude.ToString()),
                new JProperty("normalized", new JObject(
                    new JProperty("magnitude", v.normalized.magnitude.ToString()),
                    new JProperty("sqrMagnitude", v.normalized.sqrMagnitude.ToString()),
                    new JProperty("x", v.normalized.x.ToString()),
                    new JProperty("y", v.normalized.y.ToString())
                )),
                new JProperty("sqrMagnitude", v.sqrMagnitude.ToString()),
                new JProperty("x", v.x.ToString()),
                new JProperty("y", v.y.ToString()),
                new JProperty("value", v.ToString())
            );
        }

        public JObject QuaternionToJson(Quaternion q) {
            return new JObject(
                new JProperty("unity-type", typeof(Quaternion).ToString()),
                new JProperty("x", q.x),
                new JProperty("y", q.y),
                new JProperty("z", q.z),
                new JProperty("w", q.w),
                new JProperty("value", q.ToString())
            );
        }

        public JObject RectToJson(Rect r) {
            return new JObject(
                new JProperty("unity-type", typeof(Rect).ToString()),
                new JProperty("center", VectorToJson(r.center)),
                new JProperty("height", r.height.ToString()),
                new JProperty("max", VectorToJson(r.max) ),
                new JProperty("min", VectorToJson(r.min) ),
                new JProperty("position", VectorToJson(r.position) ),
                new JProperty("size", VectorToJson(r.size) ),
                new JProperty("width", r.width.ToString()),
                new JProperty("x", r.x.ToString()),
                new JProperty("xMax", r.xMax.ToString()),
                new JProperty("xMin", r.xMin.ToString()),
                new JProperty("y", r.y.ToString()),
                new JProperty("yMax", r.yMax.ToString()),
                new JProperty("yMin", r.yMin.ToString())
            );
        }
    }
}
