﻿using UnityEngine;
using Valve.Newtonsoft.Json;
using Valve.Newtonsoft.Json.Linq;
using UnityEngine.SceneManagement;

namespace ModInspector {
    class SceneResource : JsonResource {
        protected Scene scene;
        protected bool includeChildren;

        public SceneResource(Scene _scene, bool _includeChildren = false) {
            includeChildren = _includeChildren;
            scene = _scene;
            id = scene.name;
            type = "scene";
        }

        [JsonProperty("is-active-scene")]
        public bool IsActiveScene { get { return SceneManager.GetActiveScene() == scene; } }

        [JsonProperty("unity-type")]
        public string UnityType { get { return scene.GetType().ToString(); } }

        [JsonProperty("path")]
        public string Path { get { return scene.path; } }

        [JsonProperty("name")]
        public string Name { get { return scene.name; } }

        [JsonProperty("is-loaded")]
        public bool IsLoaded { get { return scene.isLoaded; } }

        [JsonProperty("build-index")]
        public int BuildIndex { get { return scene.buildIndex; } }

        [JsonProperty("is-dirty")]
        public bool IsDirty { get { return scene.isDirty; } }

        [JsonProperty("root-count")]
        public int RootCount { get { return scene.rootCount; } }

        [Relationship("children")]
        public JArray Children { get {
            JArray children = new JArray();
            
            if (includeChildren) {
                foreach (GameObject obj in scene.GetRootGameObjects()) {
                    children.Add(ToRelationship("/" + obj.name, "game-object"));
                }
            }

            return children.HasValues ? children : null;
        } }
    }
}
