﻿using Valve.Newtonsoft.Json.Linq;

namespace ModInspector
{
	public class RegisterMessage
	{
		static public string ToJSON ()
		{
			JArray message = new JArray (
				new JValue("register"),
				new JObject(
					new JProperty("user", "instance")
				)
			);

			return message.ToString ();
		}
	}
}

