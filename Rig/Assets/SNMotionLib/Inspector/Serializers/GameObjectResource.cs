﻿using UnityEngine;
using Valve.Newtonsoft.Json;
using Valve.Newtonsoft.Json.Linq;

namespace ModInspector {
    class GameObjectResource : JsonResource {
        protected GameObject obj;
        public string parentPath;

        public GameObjectResource(GameObject _obj, string _parentPath = "", string objPath = null) {
            obj = _obj;
            type = "game-object";
            parentPath = _parentPath;
            id = objPath == null ? parentPath + "/" + obj.name : objPath;
        }

        [JsonProperty("unity-type")]
        public string UnityType { get { return obj.GetType().ToString(); } }

        [JsonProperty("layer")]
        public int Layer { get { return obj.layer; } }

        [JsonProperty("scene")]
        public string Scene { get { return obj.scene.name; } }

        [JsonProperty("active-self")]
        public bool ActiveSelf { get { return obj.activeSelf; } }

        [JsonProperty("active-in-hierarchy")]
        public bool ActiveInHierarchy { get { return obj.activeInHierarchy; } }

        [JsonProperty("is-static")]
        public bool IsStatic { get { return obj.isStatic; } }

        [JsonProperty("tag")]
        public string Tag { get { return obj.tag; } }

        [JsonProperty("name")]
        public string Name { get { return obj.name; } }

        [JsonProperty("hide-flags")]
        public string HideFlags { get { return obj.hideFlags.ToString(); } }

        [Relationship("parent")]
        public JObject Parent {
            get {
                return string.IsNullOrEmpty(parentPath) ?
                    null : ToRelationship(parentPath, "game-object");
            }
        }

        [Relationship("children")]
        public JArray Children {
            get {
                JArray children = new JArray();

                foreach (Transform child in obj.transform) {
                    children.Add(ToRelationship(id + "/" + child.gameObject.name, "game-object"));
                }
                
                return children.HasValues ? children : null;
            }
        }
        
    }
}
