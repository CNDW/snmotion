﻿using UnityEngine;
using System.Reflection;
using Valve.Newtonsoft.Json;
using Valve.Newtonsoft.Json.Linq;

namespace ModInspector {
    public class CameraResource : JsonResource {
        protected Camera component;
        protected string parentPath;

		public CameraResource(Camera _component, string _parentPath) {
            id = _parentPath + "@@Camera";
            type = "game-component";
			component = _component;
			parentPath = _parentPath;
        }

        [JsonProperty("unity-type")]
        public string UnityType { get { return component.GetType().ToString(); } }

        [JsonProperty("name")]
        public string Name { get { return "Camera"; } }

        [JsonProperty("fields")]
        public JObject Fields { get {
            PropertyInfo[] fields = component.GetType().GetProperties(flags);
            JObject attributes = new JObject();
			foreach (PropertyInfo field in fields) {
                if (field.PropertyType.BaseType == typeof(MonoBehaviour)) {
					MonoBehaviour mono = (MonoBehaviour)field.GetValue(component, null);
                    if (mono != null) {
                        JObject value = new JObject(
                            new JProperty("value", mono.ToString()),
                            new JProperty("type", field.PropertyType.BaseType.ToString()),
                            new JProperty("isWritable", "False")
                        );
                    }
                } else {
                    var fieldValue = field.GetValue(component, null);

                    if (fieldValue == null) {
                        fieldValue = field.PropertyType;
                    }

                    JObject value = new JObject(

                        new JProperty("value", fieldValue.ToString()),
						new JProperty("type", field.PropertyType.BaseType.ToString()),
                        new JProperty("isWritable", "True")
                    );
                    attributes.Add(new JProperty(field.Name, value));
                }
            }
            return attributes;
        } }

        [Relationship("game-object")]
		public JObject GameObject { get { return ToRelationship(parentPath, "game-object"); } }
    }
}
