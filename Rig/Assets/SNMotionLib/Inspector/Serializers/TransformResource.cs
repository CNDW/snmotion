﻿using UnityEngine;
using Valve.Newtonsoft.Json;
using Valve.Newtonsoft.Json.Linq;

namespace ModInspector {
    public class TransformResource : JsonResource {
        protected Transform t;
        protected string parentPath;

        public TransformResource(Transform transform, string _parentPath) {
            t = transform;
            parentPath = _parentPath;
            id = parentPath + "::transform";
            type = "transform";
        }

        [JsonProperty("rotation")]
        public JObject Rotation { get { return QuaternionToJson(t.rotation); } }

        [JsonProperty("local-rotation")]
        public JObject LocalRotation { get { return QuaternionToJson(t.localRotation); } }

        [JsonProperty("local-scale")]
        public JObject LocalScale { get { return VectorToJson(t.localScale); } }

        [JsonProperty("up")]
        public JObject Up { get { return VectorToJson(t.up); } }

        [JsonProperty("right")]
        public JObject Right { get { return VectorToJson(t.right); } }

        [JsonProperty("forward")]
        public JObject Forward { get { return VectorToJson(t.forward); } }

        [JsonProperty("local-position")]
        public JObject LocalPosition { get { return VectorToJson(t.localPosition); } }

        [JsonProperty("position")]
        public JObject Position { get { return VectorToJson(t.position); } }

        [Relationship("game-object")]
        public JObject GameObject { get { return ToRelationship(parentPath, "game-object"); } }
    }
}
