﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using ModInspector;
using Valve.Newtonsoft.Json.Linq;

public class InspectorSerializerTest {

    [Test]
    public void SerializesGameObjectsWithoutError() {
		GameObject obj = new GameObject ();
		
		JObject json = InspectorSerializer.ToJson (obj);

		Assert.That((string)json["attributes"]["name"], Is.EqualTo("New Game Object"));
    }

	[Test]
	public void SerializesGameObjectTransform() {
		GameObject obj = new GameObject ();

		JObject json = InspectorSerializer.ToJson (obj.transform, obj.name);
		Assert.That(
			(string)json["attributes"]["position"]["x"].ToString(), 
			Is.EqualTo(obj.transform.position.x.ToString())
		);
	}

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator NewTestScriptWithEnumeratorPasses() {
        // Use the Assert class to test conditions.
        // yield to skip a frame
        yield return null;
    }
}
