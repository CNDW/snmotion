﻿using System;
using WebSocketSharp;
using UnityEngine;
using Valve.Newtonsoft.Json.Linq;

namespace ModInspector 
{
	public class Inspector
	{
        public static GameObject dispatcher;
		private static WebSocket _ws;
		public static WebSocket ws {
			get
			{
				if (_ws == null)
				{
					_ws = new WebSocket("ws://192.168.1.32:40510");
					_ws.Connect();
					_ws.Send(RegisterMessage.ToJSON());

                    _ws.OnMessage += InspectorActionHandler.OnMessage;

                    dispatcher = new GameObject("UnityMainThreadDispatcher");
                    dispatcher.AddComponent<UnityMainThreadDispatcher>();

                }
				return _ws;
			}
		}

        private static void _Send(string data) {
            try {
                ws.Send(data);
            } catch (Exception err) {
                Debug.Log("Failed to send ws data: " + data);
                Debug.Log(err.ToString());
            } 
        }

		public static void Send(string command, JObject payload) {
            _Send(new JArray(
                new JValue(command),
                new JObject(new JProperty("data", payload))
			).ToString());
		}

        public static void Send(string command, JArray payload) {
            _Send(new JArray(
                new JValue(command),
                new JObject(new JProperty("data", payload))
            ).ToString());
        }

	}
}
