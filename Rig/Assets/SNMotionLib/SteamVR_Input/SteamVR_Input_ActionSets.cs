// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 2.0.50727.1433
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

namespace Valve.VR
{
    using System;
    using UnityEngine;
    
    
    public partial class SteamVR_Input
    {
        
        private static SteamVR_Input_ActionSet_default p__default;
        
        private static SteamVR_Input_ActionSet_platformer p_platformer;
        
        private static SteamVR_Input_ActionSet_buggy p_buggy;
        
        public static SteamVR_Input_ActionSet_default _default
        {
            get
            {
                return Valve.VR.SteamVR_Input.p__default.GetCopy <SteamVR_Input_ActionSet_default>();
            }
        }
        
        public static SteamVR_Input_ActionSet_platformer platformer
        {
            get
            {
                return Valve.VR.SteamVR_Input.p_platformer.GetCopy <SteamVR_Input_ActionSet_platformer>();
            }
        }
        
        public static SteamVR_Input_ActionSet_buggy buggy
        {
            get
            {
                return Valve.VR.SteamVR_Input.p_buggy.GetCopy <SteamVR_Input_ActionSet_buggy>();
            }
        }
        
        public static void StartPreInitActionSets()
        {
            Valve.VR.SteamVR_Input.p__default = ((SteamVR_Input_ActionSet_default)(SteamVR_ActionSet.Create <SteamVR_Input_ActionSet_default>("/actions/default")));
            Valve.VR.SteamVR_Input.p_platformer = ((SteamVR_Input_ActionSet_platformer)(SteamVR_ActionSet.Create <SteamVR_Input_ActionSet_platformer>("/actions/platformer")));
            Valve.VR.SteamVR_Input.p_buggy = ((SteamVR_Input_ActionSet_buggy)(SteamVR_ActionSet.Create <SteamVR_Input_ActionSet_buggy>("/actions/buggy")));
            Valve.VR.SteamVR_Input.actionSets = new Valve.VR.SteamVR_ActionSet[]
            {
                    Valve.VR.SteamVR_Input._default,
                    Valve.VR.SteamVR_Input.platformer,
                    Valve.VR.SteamVR_Input.buggy};
        }
    }
}
