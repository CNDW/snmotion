﻿// SDK Manager|Utilities|90010
namespace VRTK
{
    using UnityEngine;
    using UnityEngine.XR;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// The SDK Manager script provides configuration of supported SDKs and manages a list of VRTK_SDKSetups to use.
    /// </summary>
    public sealed class VRTK_SDKManager : MonoBehaviour
    {

        /// <summary>
        /// Event Payload
        /// </summary>
        /// <param name="previousSetup">The previous loaded Setup. `null` if no previous Setup was loaded.</param>
        /// <param name="currentSetup">The current loaded Setup. `null` if no Setup is loaded anymore. See `errorMessage` to check whether this is `null` because of an error.</param>
        /// <param name="errorMessage">Explains why loading a list of Setups wasn't successful if `currentSetup` is `null` and an error occurred. `null` if no error occurred.</param>
        public struct LoadedSetupChangeEventArgs
        {
            public readonly VRTK_SDKSetup previousSetup;
            public readonly VRTK_SDKSetup currentSetup;
            public readonly string errorMessage;

            public LoadedSetupChangeEventArgs(VRTK_SDKSetup previousSetup, VRTK_SDKSetup currentSetup, string errorMessage)
            {
                this.previousSetup = previousSetup;
                this.currentSetup = currentSetup;
                this.errorMessage = errorMessage;
            }
        }

        /// <summary>
        /// Event Payload
        /// </summary>
        /// <param name="sender">this object</param>
        /// <param name="e"><see cref="LoadedSetupChangeEventArgs"/></param>
        public delegate void LoadedSetupChangeEventHandler(VRTK_SDKManager sender, LoadedSetupChangeEventArgs e);

        private static VRTK_SDKManager _instance;
        public static VRTK_SDKManager instance
        {
            get
            {
                if (_instance == null)
                {
                    VRTK_SDKManager sdkManager = VRTK_SharedMethods.FindEvenInactiveComponent<VRTK_SDKManager>(true);
                    if (sdkManager != null)
                    {
                        sdkManager.CreateInstance();
                    }
                }

                return _instance;
            }
        }

        /// <summary>
        /// A collection of behaviours to toggle on loaded setup change.
        /// </summary>
        public static HashSet<Behaviour> delayedToggleBehaviours = new HashSet<Behaviour>();

        /// <summary>
        /// The ValidInstance method returns whether the SDK Manager isntance is valid (i.e. it's not null).
        /// </summary>
        /// <returns>Returns `true` if the SDK Manager instance is valid or returns `false` if it is null.</returns>
        public static bool ValidInstance()
        {
            return (instance != null);
        }

        /// <summary>
        /// The AttemptAddBehaviourToToggleOnLoadedSetupChange method will attempt to add the given behaviour to the loaded setup change toggle if the SDK Manager instance exists. If it doesn't exist then it adds it to the `delayedToggleBehaviours` HashSet to be manually added later with the `ProcessDelayedToggleBehaviours` method.
        /// </summary>
        /// <param name="givenBehaviour">The behaviour to add.</param>
        /// <returns>Returns `true` if the SDK Manager instance was valid.</returns>
        public static bool AttemptAddBehaviourToToggleOnLoadedSetupChange(Behaviour givenBehaviour)
        {
            if (ValidInstance())
            {
                instance.AddBehaviourToToggleOnLoadedSetupChange(givenBehaviour);
                return true;
            }
            delayedToggleBehaviours.Add(givenBehaviour);
            return false;
        }

        /// <summary>
        /// The AttemptRemoveBehaviourToToggleOnLoadedSetupChange method will attempt to remove the given behaviour from the loaded setup change toggle if the SDK Manager instance exists.
        /// </summary>
        /// <param name="givenBehaviour">The behaviour to remove.</param>
        /// <returns>Returns `true` if the SDK Manager instance was valid.</returns>
        public static bool AttemptRemoveBehaviourToToggleOnLoadedSetupChange(Behaviour givenBehaviour)
        {
            if (ValidInstance())
            {
                instance.RemoveBehaviourToToggleOnLoadedSetupChange(givenBehaviour);
                delayedToggleBehaviours.Remove(givenBehaviour);
                return true;
            }
            return false;
        }

        /// <summary>
        /// The ProcessDelayedToggleBehaviours method will attempt to addd the behaviours in the `delayedToggleBehaviours` HashSet to the loaded setup change toggle.
        /// </summary>
        public static void ProcessDelayedToggleBehaviours()
        {
            if (ValidInstance())
            {
                foreach (Behaviour currentBehaviour in new HashSet<Behaviour>(delayedToggleBehaviours))
                {
                    instance.AddBehaviourToToggleOnLoadedSetupChange(currentBehaviour);
                }
                delayedToggleBehaviours.Clear();
            }
        }

        /// <summary>
        /// The SubscribeLoadedSetupChanged method attempts to register the given callback with the `LoadedSetupChanged` event.
        /// </summary>
        /// <param name="callback">The callback to register.</param>
        /// <returns>Returns `true` if the SDK Manager instance was valid.</returns>
        public static bool SubscribeLoadedSetupChanged(LoadedSetupChangeEventHandler callback)
        {
            if (ValidInstance())
            {
                instance.LoadedSetupChanged += callback;
                return true;
            }
            return false;
        }

        /// <summary>
        /// The UnsubscribeLoadedSetupChanged method attempts to unregister the given callback from the `LoadedSetupChanged` event. 
        /// </summary>
        /// <param name="callback">The callback to unregister.</param>
        /// <returns>Returns `true` if the SDK Manager instance was valid.</returns>
        public static bool UnsubscribeLoadedSetupChanged(LoadedSetupChangeEventHandler callback)
        {
            if (ValidInstance())
            {
                instance.LoadedSetupChanged -= callback;
                return true;
            }
            return false;
        }

        /// <summary>
        /// The GetLoadedSDKSetup method returns the current loaded SDK Setup for the SDK Manager instance.
        /// </summary>
        /// <returns>Returns `true` if the SDK Manager instance was valid.</returns>
        public static VRTK_SDKSetup GetLoadedSDKSetup()
        {
            if (ValidInstance())
            {
                return instance.loadedSetup;
            }
            return null;
        }


        [Tooltip("A reference to the GameObject that contains any scripts that apply to the Left Hand Controller.")]
        public GameObject scriptAliasLeftController;
        [Tooltip("A reference to the GameObject that contains any scripts that apply to the Right Hand Controller.")]
        public GameObject scriptAliasRightController;
        public bool autoLoadSetup = true;
        [Tooltip("The SDK Setup to use")]
        public VRTK_SDKSetup setup;

        /// <summary>
        /// The loaded SDK Setup. `null` if no setup is currently loaded.
        /// </summary>
        public VRTK_SDKSetup loadedSetup
        {
            get
            {
                if (_loadedSetup == null)
                {
                    _loadedSetup = setup;
                }

                return _loadedSetup;
            }
            private set { _loadedSetup = value; }
        }

        private VRTK_SDKSetup _loadedSetup;
        private static HashSet<VRTK_SDKInfo> _previouslyUsedSetupInfos = new HashSet<VRTK_SDKInfo>();

        /// <summary>
        /// All behaviours that need toggling whenever `loadedSetup` changes.
        /// </summary>
        public ReadOnlyCollection<Behaviour> behavioursToToggleOnLoadedSetupChange { get; private set; }
        private List<Behaviour> _behavioursToToggleOnLoadedSetupChange = new List<Behaviour>();
        private Dictionary<Behaviour, bool> _behavioursInitialState = new Dictionary<Behaviour, bool>();
        private Coroutine checkLeftControllerReadyRoutine = null;
        private Coroutine checkRightControllerReadyRoutine = null;
        private float checkControllerReadyDelay = 1f;
        private int checkControllerValidTimer = 50;
        /// <summary>
        /// The event invoked whenever the loaded SDK Setup changes.
        /// </summary>
        public event LoadedSetupChangeEventHandler LoadedSetupChanged;


        /// <summary>
        /// The AddBehaviourToToggleOnLoadedSetupChange method adds a behaviour to the list of behaviours to toggle when `loadedSetup` changes.
        /// </summary>
        /// <param name="behaviour">The behaviour to add.</param>
        public void AddBehaviourToToggleOnLoadedSetupChange(Behaviour behaviour)
        {
            if (!_behavioursToToggleOnLoadedSetupChange.Contains(behaviour))
            {
                _behavioursToToggleOnLoadedSetupChange.Add(behaviour);
                _behavioursInitialState.Add(behaviour, behaviour.enabled);
            }

            if (loadedSetup == null && behaviour.enabled)
            {
                behaviour.enabled = false;
            }
        }

        /// <summary>
        /// The RemoveBehaviourToToggleOnLoadedSetupChange method removes a behaviour of the list of behaviours to toggle when `loadedSetup` changes.
        /// </summary>
        /// <param name="behaviour">The behaviour to remove.</param>
        public void RemoveBehaviourToToggleOnLoadedSetupChange(Behaviour behaviour)
        {
            _behavioursToToggleOnLoadedSetupChange.Remove(behaviour);
        }


        /// <summary>
        /// The TryLoadSDKSetup method tries to load a valid VRTK_SDKSetup from a list.
        /// </summary>
        /// <remarks>
        /// The first loadable VRTK_SDKSetup in the list will be loaded. Will fall back to disable VR if none of the provided Setups is useable.
        /// </remarks>
        public void TryLoadSDKSetup()
        {
            VRTK_SDKSetup previousLoadedSetup = loadedSetup;
            ToggleBehaviours(false);
            loadedSetup = null;
            if (previousLoadedSetup != null)
            {
                previousLoadedSetup.OnUnloaded(this);
            }

            string loadedDeviceName = string.IsNullOrEmpty(XRSettings.loadedDeviceName) ? "None" : XRSettings.loadedDeviceName;
            bool isDeviceAlreadyLoaded = setup.usedVRDeviceNames.Contains(loadedDeviceName);
            if (!isDeviceAlreadyLoaded)
            {
                string[] vrDeviceNames = setup.usedVRDeviceNames
                    .Distinct()
                    .Concat(new[] { "None" }) // Add "None" to the end to fall back to
                    .ToArray();
                XRSettings.LoadDeviceByName(vrDeviceNames);
            }

            StartCoroutine(FinishSDKSetupLoading(previousLoadedSetup));
        }

        /// <summary>
        /// The UnloadSDKSetup method unloads the currently loaded VRTK_SDKSetup, if there is one.
        /// </summary>
        /// <param name="disableVR">Whether to disable VR altogether after unloading the SDK Setup.</param>
        public void UnloadSDKSetup(bool disableVR = false)
        {
            if (loadedSetup != null)
            {
                ToggleBehaviours(false);
            }

            VRTK_SDKSetup previousLoadedSetup = loadedSetup;
            loadedSetup = null;

            if (previousLoadedSetup != null)
            {
                previousLoadedSetup.OnUnloaded(this);
            }

            if (disableVR)
            {
                XRSettings.LoadDeviceByName("None");
                XRSettings.enabled = false;
            }

            if (previousLoadedSetup != null)
            {
                OnLoadedSetupChanged(new LoadedSetupChangeEventArgs(previousLoadedSetup, null, null));
            }

            _previouslyUsedSetupInfos.Clear();
            if (previousLoadedSetup != null)
            {
                _previouslyUsedSetupInfos.UnionWith(
                    new[]
                    {
                        previousLoadedSetup.systemSDKInfo,
                        previousLoadedSetup.boundariesSDKInfo,
                        previousLoadedSetup.headsetSDKInfo,
                        previousLoadedSetup.controllerSDKInfo
                    }
                );
            }
        }

        static VRTK_SDKManager()
        {
        }

        private void OnEnable()
        {
            behavioursToToggleOnLoadedSetupChange = _behavioursToToggleOnLoadedSetupChange.AsReadOnly();

            CreateInstance();

            if (loadedSetup == null && autoLoadSetup)
            {
                TryLoadSDKSetup();
            }
        }

        private void OnDisable()
        {
            if (checkLeftControllerReadyRoutine != null)
            {
                StopCoroutine(checkLeftControllerReadyRoutine);
            }

            if (checkRightControllerReadyRoutine != null)
            {
                StopCoroutine(checkRightControllerReadyRoutine);
            }
        }

        private void CreateInstance()
        {
            if (_instance == null)
            {
                _instance = this;
                VRTK_SDK_Bridge.InvalidateCaches();
            }
            else if (_instance != this)
            {
                Destroy(gameObject);
            }
        }

        private void OnLoadedSetupChanged(LoadedSetupChangeEventArgs e)
        {
            LoadedSetupChangeEventHandler handler = LoadedSetupChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private IEnumerator FinishSDKSetupLoading(VRTK_SDKSetup previousLoadedSetup)
        {
            yield return null;

            string loadedDeviceName = string.IsNullOrEmpty(XRSettings.loadedDeviceName) ? "None" : XRSettings.loadedDeviceName;
            loadedSetup = setup;

            if (loadedSetup == null)
            {
                // The loaded VR Device doesn't match any SDK Setup
                UnloadSDKSetup();

                const string errorMessage = "No SDK Setup from the provided list could be loaded.";
                OnLoadedSetupChanged(new LoadedSetupChangeEventArgs(previousLoadedSetup, null, errorMessage));
                VRTK_Logger.Error(errorMessage);

                yield break;
            }

            if (loadedSetup.usedVRDeviceNames.Except(new[] { "None" }).Any())
            {
                // The loaded VR Device is actually a VR Device
                XRSettings.enabled = true;

                if (!XRDevice.isPresent)
                {
                    // Despite being loaded, the loaded VR Device isn't working correctly
                    string errorMessage = "An SDK Setup from the provided list could be loaded, but the device is not in working order.";

                    ToggleBehaviours(false);
                    loadedSetup = null;

                    // There are no other SDK Setups
                    UnloadSDKSetup();

                    OnLoadedSetupChanged(new LoadedSetupChangeEventArgs(previousLoadedSetup, null, errorMessage));
                    VRTK_Logger.Error(errorMessage);

                    yield break;
                }
            }

            // A VR Device was correctly loaded, is working and matches an SDK Setup
            loadedSetup.OnLoaded(this);
            ToggleBehaviours(true);
            CheckControllersReady();
            OnLoadedSetupChanged(new LoadedSetupChangeEventArgs(previousLoadedSetup, loadedSetup, null));
        }

        private void CheckControllersReady()
        {
            if (checkLeftControllerReadyRoutine != null)
            {
                StopCoroutine(checkLeftControllerReadyRoutine);
            }
            checkLeftControllerReadyRoutine = StartCoroutine(CheckLeftControllerReady());

            if (checkRightControllerReadyRoutine != null)
            {
                StopCoroutine(checkRightControllerReadyRoutine);
            }
            checkRightControllerReadyRoutine = StartCoroutine(CheckRightControllerReady());
        }

        private IEnumerator CheckLeftControllerReady()
        {
            WaitForSeconds delayInstruction = new WaitForSeconds(checkControllerReadyDelay);
            int maxCheckTime = checkControllerValidTimer;
            while (true)
            {
                if (loadedSetup != null && loadedSetup.actualLeftController != null && loadedSetup.actualLeftController.activeInHierarchy && (loadedSetup.controllerSDK.GetCurrentControllerType() != SDK_BaseController.ControllerType.Undefined || maxCheckTime < 0))
                {
                    break;
                }
                maxCheckTime--;
                yield return delayInstruction;
            }
            loadedSetup.controllerSDK.OnControllerReady(SDK_BaseController.ControllerHand.Left);
        }

        private IEnumerator CheckRightControllerReady()
        {
            WaitForSeconds delayInstruction = new WaitForSeconds(checkControllerReadyDelay);
            int maxCheckTime = checkControllerValidTimer;
            while (true)
            {
                if (loadedSetup != null && loadedSetup.actualRightController != null && loadedSetup.actualRightController.activeInHierarchy && (loadedSetup.controllerSDK.GetCurrentControllerType() != SDK_BaseController.ControllerType.Undefined || maxCheckTime < 0))
                {
                    break;
                }
                maxCheckTime--;
                yield return delayInstruction;
            }
            loadedSetup.controllerSDK.OnControllerReady(SDK_BaseController.ControllerHand.Right);
        }

        private void ToggleBehaviours(bool state)
        {
            List<Behaviour> listCopy = _behavioursToToggleOnLoadedSetupChange.ToList();
            if (!state)
            {
                listCopy.Reverse();
            }

            for (int index = 0; index < listCopy.Count; index++)
            {
                Behaviour behaviour = listCopy[index];
                if (behaviour == null)
                {
                    VRTK_Logger.Error(string.Format("A behaviour to toggle has been destroyed. Have you forgot the corresponding call `VRTK_SDKManager.instance.RemoveBehaviourToToggleOnLoadedSetupChange(this)` in the `OnDestroy` method of `{0}`?", behaviour.GetType()));
                    _behavioursToToggleOnLoadedSetupChange.RemoveAt(state ? index : _behavioursToToggleOnLoadedSetupChange.Count - 1 - index);

                    continue;
                }

                behaviour.enabled = (state && _behavioursInitialState.ContainsKey(behaviour) ? _behavioursInitialState[behaviour] : state);
            }
        }
    }
}