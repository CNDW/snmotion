﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

namespace SNMotion { 
	
	public class SNMotion_ActionSets : MonoBehaviour {

		public SteamVR_ActionSet defaultActionSet = SteamVR_Input.GetActionSet("default");
		public SteamVR_ActionSet platformActionSet = SteamVR_Input.GetActionSet("platformer");

		public SteamVR_Input_Sources forSources = SteamVR_Input_Sources.Any;



		private void Start()
		{
			Debug.Log(string.Format("[SteamVR] Activating {0} action set.", defaultActionSet.fullPath));
			defaultActionSet.Activate(forSources, 0, false);
			Debug.Log(string.Format("[SteamVR] Activating {0} action set.", platformActionSet.fullPath));
			platformActionSet.Activate(forSources, 0, false);
		}

		private void OnDestroy()
		{
			Debug.Log(string.Format("[SteamVR] Deactivating {0} action set.", defaultActionSet.fullPath));
			defaultActionSet.Deactivate(forSources);
			Debug.Log(string.Format("[SteamVR] Deactivating {0} action set.", platformActionSet.fullPath));
			platformActionSet.Deactivate(forSources);
		}
	}
}