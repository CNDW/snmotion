﻿using System.Collections;
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

using Valve.VR;
using Valve.VR.InteractionSystem;

namespace SNMotion {
	public class SNMotion_Player : Valve.VR.InteractionSystem.Player {
        public enum UIModes {
            Menu,
            FirstPerson
        }

		public SNMotion_Hand _leftHand;
		public SNMotion_Hand _rightHand;
        public Camera camera;

		private static SNMotion_Player _main;
		public static SNMotion_Player main {
			get {
				if (_main == null) {
					_main = (SNMotion_Player)Valve.VR.InteractionSystem.Player.instance;
				}
				return _main;
			}
		}

		private static BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;

        public static Action<SNMotion_Hand> OnTriggerDown;

		public static SNMotion_Hand HandAtIndex(uint index) {
			SteamVR_Input_Sources source = (SteamVR_Input_Sources)index;
			if (main._leftHand.handType == source) {
				return main._leftHand;
			} else if (main._rightHand.handType == source) {
				return main._rightHand;
			} else {
				throw new Exception ("Unable to resolve input with index: " + index.ToString ());
			}
		}

		public Vector2 GetHandMoveAxis(string hand) {
			if (hand == "LeftHand") {
				return _leftHand.move.GetAxis (Valve.VR.SteamVR_Input_Sources.LeftHand);
			} else {
				return _rightHand.move.GetAxis (Valve.VR.SteamVR_Input_Sources.RightHand);
			}
		}

        public bool GetHandTriggerDown(string hand) {
            if (hand == "LeftHand") {
                return _leftHand.uiInteractAction.GetStateDown(Valve.VR.SteamVR_Input_Sources.LeftHand);
            } else {
                return _rightHand.uiInteractAction.GetStateDown(Valve.VR.SteamVR_Input_Sources.RightHand);
            }
        }

        public bool GetHandTriggerUp(string hand) {
            if (hand == "LeftHand") {
                return _leftHand.uiInteractAction.GetStateUp(Valve.VR.SteamVR_Input_Sources.LeftHand);
            } else {
                return _rightHand.uiInteractAction.GetStateUp(Valve.VR.SteamVR_Input_Sources.RightHand);
            }
        }

        public bool GetHandTriggerHeld(string hand) {
            if (hand == "LeftHand") {
                return _leftHand.uiInteractAction.GetState(Valve.VR.SteamVR_Input_Sources.LeftHand);
            } else {
                return _rightHand.uiInteractAction.GetState(Valve.VR.SteamVR_Input_Sources.RightHand);
            }
        }

        // Use this for initialization
        new protected IEnumerator Start () {
			rigSteamVR = transform.Find ("SteamVRObjects").gameObject;
			rig2DFallback = transform.Find ("NoSteamVRFallbackObjects").gameObject;

			Transform mainCamera = transform.Find("MainCamera");
			if (mainCamera != null) {
				hmdTransforms = new Transform[] { mainCamera };
			}

			Hand leftHand = rigSteamVR.transform.Find ("LeftHand").gameObject.GetComponent<Hand> ();
			Hand rightHand = rigSteamVR.transform.Find ("RightHand").gameObject.GetComponent<Hand> ();
			hands = new Hand[]{ leftHand, rightHand };
			_leftHand = (SNMotion_Hand)leftHand;
			_rightHand = (SNMotion_Hand)rightHand;
            SetMode(currentMode);
			InvokeMethod ("Enter" + currentMode.ToString () + "Mode");
			trackingOriginTransform = transform;

            return base.Start ();
		}

        new protected void Update() {
			if (SteamVR.initializedState != SteamVR.InitializedStates.InitializeSuccess)
				return;

			// TODO: Figure out how to make this work
//			if (headsetOnHead.GetStateDown (SteamVR_Input_Sources.Head)) {
//				Debug.Log ("<b>SteamVR Interaction System</b> Headset placed on head");
//			} else if (headsetOnHead.GetStateUp (SteamVR_Input_Sources.Head)) {
//				Debug.Log ("<b>SteamVR Interaction System</b> Headset removed");
//			} else {
//				Debug.Log ("<b>SteamVR Interaction System</b> No Headset Data");
//			}

			#if UNITY_EDITOR
			if (CURRENT_MODE != currentMode) {
				currentMode = CURRENT_MODE;
			}
			#endif

            base.Update();
            if (currentMode == UIModes.Menu) {
                MenuModeUpdate();
            }
        }

        private void MenuModeUpdate() {

        }

        public void EnterMenuMode() {

        }

        public void ExitMenuMode() {

        }

        public void EnterFirstPersonMode() {

        }


		private void InvokeMethod(string methodName) {
			foreach(Hand _hand in hands) {
				SNMotion_Hand hand = (SNMotion_Hand)_hand;
				MethodInfo info = hand.GetType ().GetMethod (methodName, flags);
				if (info != null) {
					info.Invoke (hand, null);
				}
			}

            MethodInfo baseInfo = GetType().GetMethod(methodName, flags);
            if (baseInfo != null) {
                baseInfo.Invoke(this, null);
            } else {
                Debug.Log("No method found for " + methodName + ": aborting invoke for player object");
            }
        }

		private UIModes _currentMode;
		public UIModes currentMode {
			get { return _currentMode; }
			set { SetUIMode(value); }
		}

		#if UNITY_EDITOR
		[SerializeField]
		private UIModes CURRENT_MODE;
		#endif
        private void SetMode(UIModes mode) {
			#if UNITY_EDITOR
			CURRENT_MODE = mode;
			#endif
			_currentMode = mode;
            foreach (Hand _hand in hands) {
                SNMotion_Hand hand = (SNMotion_Hand)_hand;
                hand.SetMode(mode);
            }
        }

        public void SetUIMode(UIModes newMode) {
			if (newMode != currentMode) {
				string exitMethod = "Exit" + currentMode.ToString () + "Mode";
				InvokeMethod (exitMethod);

				string enterMethod = "Enter" + newMode.ToString () + "Mode";
				InvokeMethod (enterMethod);

                SetMode(newMode);
            }
		}
	}
}
