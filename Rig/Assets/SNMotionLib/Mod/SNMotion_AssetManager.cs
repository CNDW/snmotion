﻿using System;
using System.IO;
using UnityEngine;

namespace SNMotion
{    
	public class SNMotion_AssetManager : ScriptableObject
	{
		private static AssetBundle _assets;
		public static AssetBundle assets
		{
			get
			{
				if (_assets == null) {
#if UNITY_EDITOR
					_assets = AssetBundle.LoadFromFile (Path.Combine (Application.streamingAssetsPath, "snmotion"));
#else
					_assets = AssetBundle.LoadFromFile("./QMods/SNMotion/snmotion");
#endif
					if (_assets == null) {
						Debug.Log("Failed to load AssetBundle!");
					}
				}

				return _assets;
			}
		}
	}
}

