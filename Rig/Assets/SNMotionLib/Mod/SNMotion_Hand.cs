﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

using Valve.VR;
using Valve.VR.InteractionSystem;
using Valve.VR.Extras;
using VRTK;

namespace SNMotion {
	public class SNMotion_Hand : Valve.VR.InteractionSystem.Hand {
		public Transform fingerPointer;
		public Transform laserPointer;
		public bool togglePointerOnHit = false;

		[SerializeField]
        private SNMotion_Player.UIModes currentMode;

        public SteamVR_Action_Vector2 move = SteamVR_Input.GetAction<SteamVR_Action_Vector2>("Move");
        public SteamVR_Action_Boolean jump = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("Jump");

        public void SetMode(SNMotion_Player.UIModes mode) {
            currentMode = mode;
        }

        private SNMotion_Player _player;
        public SNMotion_Player player {
            get {
                if (_player == null) {
                    _player = gameObject.GetComponentInParent<SNMotion_Player>();
                }
                return _player;
            }
        }

		override protected void Awake() {
			handType = transform.name == "LeftHand" ?
				SteamVR_Input_Sources.LeftHand :
				SteamVR_Input_Sources.RightHand;
			objectAttachmentPoint = transform.Find ("ObjectAttachmentPoint");
			hoverSphereTransform = transform.Find ("HoverPoint");
			string renderModelAsset = transform.name == "LeftHand" ?
				"LeftRenderModel Slim" : "RightRenderModel Slim";
			renderModelPrefab = SNMotion_AssetManager.assets.LoadAsset (renderModelAsset) as GameObject;

			applicationLostFocusObject = new GameObject("_application_lost_focus");
			applicationLostFocusObject.transform.parent = transform;
			applicationLostFocusObject.SetActive(false);

			Transform otherHandTransform = transform.name == "LeftHand" ?
				transform.parent.Find ("RightHand") :
				transform.parent.Find ("LeftHand");

			otherHand = otherHandTransform.gameObject.GetComponent<Hand> ();
			inputFocusAction = SteamVR_Events.InputFocusAction(OnInputFocus);
		}

		new protected IEnumerator Start() {
			trackedObject = gameObject.AddComponent<SteamVR_Behaviour_Pose>();
			trackedObject.inputSource = handType;
			trackedObject.poseAction = SteamVR_Input.GetAction<SteamVR_Action_Pose> ("Pose");
			trackedObject.onTransformUpdated.AddListener (OnTransformUpdated);
			laserPointer = transform.Find("LaserPointer");
			fingerPointer = transform.Find("FingerPointer");
			SetupEvents ();
			return base.Start ();
		}

		private void SetupEvents() {
			VRTK_UIPointer pointer = GetComponentInChildren<VRTK_UIPointer> ();
			if (pointer == null)
			{
//				VRTK_Logger.Error(VRTK_Logger.GetCommonMessage(VRTK_Logger.CommonMessageKeys.REQUIRED_COMPONENT_MISSING_FROM_GAMEOBJECT, "VRTK_ControllerUIPointerEvents_ListenerExample", "VRTK_UIPointer", "the Controller Alias"));
				return;
			}

			if (togglePointerOnHit)
			{
				pointer.activationMode = VRTK_UIPointer.ActivationMethods.AlwaysOn;
			}

			//Setup controller event listeners
//			pointer.UIPointerElementEnter += VRTK_ControllerUIPointerEvents_ListenerExample_UIPointerElementEnter;
//			pointer.UIPointerElementExit += VRTK_ControllerUIPointerEvents_ListenerExample_UIPointerElementExit;
//			pointer.UIPointerElementClick += VRTK_ControllerUIPointerEvents_ListenerExample_UIPointerElementClick;
//			pointer.UIPointerElementDragStart += VRTK_ControllerUIPointerEvents_ListenerExample_UIPointerElementDragStart;
//			pointer.UIPointerElementDragEnd += VRTK_ControllerUIPointerEvents_ListenerExample_UIPointerElementDragEnd;
		}

		override protected void OnEnable() {
			inputFocusAction.enabled = true;

			float hoverUpdateBegin = ((otherHand != null) && (otherHand.GetInstanceID() < GetInstanceID())) ? (0.5f * hoverUpdateInterval) : (0.0f);
			InvokeRepeating("UpdateHovering", hoverUpdateBegin, hoverUpdateInterval);
			InvokeRepeating("UpdateDebugText", hoverUpdateBegin, hoverUpdateInterval);
		}

		public void EnterMenuMode() {
			if (fingerPointer != null) {
				fingerPointer.gameObject.SetActive(false);
			}
			ShowController ();
			SetSkeletonRangeOfMotion(EVRSkeletalMotionRange.WithController);
            if (laserPointer != null) {
                laserPointer.gameObject.SetActive(true);
            }
        }

		public void ExitMenuMode() {
		}

        public void EnterFirstPersonMode() {
            HideController();
			SetSkeletonRangeOfMotion(EVRSkeletalMotionRange.WithoutController);
			if (laserPointer != null) {
				laserPointer.gameObject.SetActive(false);
			}
			if (fingerPointer != null) {
				fingerPointer.gameObject.SetActive(true);
			}
        }

		public void ExitFirstPersonMode() {
        }

		public Vector2 GetTouchpadAxis() {
			return move.GetAxis (handType);
		}

		public bool GetTriggerState() {
			return grabPinchAction.GetState (handType);
		}

		public bool GetGripState() {
			return grabGripAction.GetState (handType);
		}

		public bool GetTouchpadState() {
			return jump.GetState (handType);
		}

        new protected void Update() {
            base.Update();
            Vector2 data = move.GetAxis(handType);
            //Debug.Log("Move state: " + "x: " + data.x + " y: " + data.y); // trackpad touch location
            //Debug.Log("Jump state: " + jump.GetStateDown(handType).ToString()); //trackpad click
            //Debug.Log("Pinch state: " + grabPinchAction.GetStateDown(handType).ToString()); //trigger click
            //Debug.Log("Grip state: " + grabGripAction.GetStateDown(handType).ToString()); //grip click
            if (currentMode == SNMotion_Player.UIModes.Menu) {
                MenuModeUpdate();
            }
        }

        protected void MenuModeUpdate() {
            if (uiInteractAction.GetStateDown(handType)) {
                ExecuteEvents.Execute<IPointerClickHandler>(null, new PointerEventData(EventSystem.current), ExecuteEvents.pointerClickHandler);
                if (SNMotion_Player.OnTriggerDown != null) {
                    SNMotion_Player.OnTriggerDown(this);
                }
            }
		}

		private void VRTK_ControllerUIPointerEvents_ListenerExample_UIPointerElementEnter(object sender, UIPointerEventArgs e)
		{
			VRTK_Logger.Info("UI Pointer entered " + e.currentTarget.name + " on Controller index [" + VRTK_ControllerReference.GetRealIndex(e.controllerReference) + "] and the state was " + e.isActive + " ### World Position: " + e.raycastResult.worldPosition);
			if (togglePointerOnHit && GetComponent<VRTK_Pointer>())
			{
				GetComponent<VRTK_Pointer>().Toggle(true);
			}
		}

		private void VRTK_ControllerUIPointerEvents_ListenerExample_UIPointerElementExit(object sender, UIPointerEventArgs e)
		{
			VRTK_Logger.Info("UI Pointer exited " + e.previousTarget.name + " on Controller index [" + VRTK_ControllerReference.GetRealIndex(e.controllerReference) + "] and the state was " + e.isActive);
			if (togglePointerOnHit && GetComponent<VRTK_Pointer>())
			{
				GetComponent<VRTK_Pointer>().Toggle(false);
			}
		}

		private void VRTK_ControllerUIPointerEvents_ListenerExample_UIPointerElementClick(object sender, UIPointerEventArgs e)
		{
			VRTK_Logger.Info("UI Pointer clicked " + e.currentTarget.name + " on Controller index [" + VRTK_ControllerReference.GetRealIndex(e.controllerReference) + "] and the state was " + e.isActive + " ### World Position: " + e.raycastResult.worldPosition);
		}

		private void VRTK_ControllerUIPointerEvents_ListenerExample_UIPointerElementDragStart(object sender, UIPointerEventArgs e)
		{
			VRTK_Logger.Info("UI Pointer started dragging " + e.currentTarget.name + " on Controller index [" + VRTK_ControllerReference.GetRealIndex(e.controllerReference) + "] and the state was " + e.isActive + " ### World Position: " + e.raycastResult.worldPosition);
		}

		private void VRTK_ControllerUIPointerEvents_ListenerExample_UIPointerElementDragEnd(object sender, UIPointerEventArgs e)
		{
			VRTK_Logger.Info("UI Pointer stopped dragging " + e.currentTarget.name + " on Controller index [" + VRTK_ControllerReference.GetRealIndex(e.controllerReference) + "] and the state was " + e.isActive + " ### World Position: " + e.raycastResult.worldPosition);
		}
	}
}
