﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace SNMotion {
    class SNMotion_MenuSelectable : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {
        public bool isSelected = false;
        
        protected virtual void OnEnable() {
            SNMotion_Player.OnTriggerDown += OnTriggerDown;
        }

        protected virtual void OnDisable() {
            SNMotion_Player.OnTriggerDown -= OnTriggerDown;
        }

        public void OnPointerEnter (PointerEventData data) {
            isSelected = true;
        }

        public void OnPointerExit (PointerEventData data) {
            isSelected = false;
        }

        private void OnTriggerDown(SNMotion_Hand hand) {
            if (isSelected == true) {
                Button button = gameObject.GetComponent<Button>();
                button.onClick.Invoke();
            }
        }
    }
}
