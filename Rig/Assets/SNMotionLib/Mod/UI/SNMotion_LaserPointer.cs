﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SNMotion {
	public class SNMotion_LaserPointer : MonoBehaviour {

		public Transform beam;
		public MeshRenderer beamMesh;

		public float maxDistance = 100f;
		public float beamWidth = 0.0025f;
		public Material defaultColor;
		public Material activatedColor;
		public Vector3 localPoint;

		// Use this for initialization
		void Start () {
		}
		
		// Update is called once per frame
		void Update () {
			RaycastHit hit;
			if (Physics.Raycast (transform.position, transform.forward, out hit, maxDistance)) {
				Canvas target = hit.collider.GetComponent<Canvas> ();
				if (target) {
					float distance = (hit.transform.position - beam.position).magnitude + 0.1f;
					beam.localPosition = new Vector3 (0f, 0f, distance);
					beam.localScale = new Vector3 (beamWidth, distance, beamWidth);
					beamMesh.material = activatedColor;
				}
			} else {
				beam.localPosition = new Vector3 (0f, 0f, maxDistance);
				beam.localScale = new Vector3 (beamWidth, maxDistance, beamWidth);
				beamMesh.material = defaultColor;
			}
		}
	}
}
