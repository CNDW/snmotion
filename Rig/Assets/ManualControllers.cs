﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using SNMotion;

public class ManualControllers : MonoBehaviour {
	GameObject lController;
	GameObject rController;
	GameObject lController_prefab;
	GameObject rController_prefab;
	// Use this for initialization
	void Start () {
		Debug.Log ("Initializing controllers from prefab");
		Camera _camera = GameObject.FindObjectOfType<Camera> ();

		GameObject _prefab = SNMotion_AssetManager.assets.LoadAsset ("SNMotionPlayer") as GameObject;
		GameObject controllers = Instantiate(_prefab, _camera.transform.parent);

		Debug.Log ("Controller Initialization complete");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
