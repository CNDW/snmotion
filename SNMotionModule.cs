using CNDW.Harmony;
using UnityEngine;
using Valve.VR;
using System.Reflection;
using UnityEngine.SceneManagement;

namespace SNMotion
{

    public class SNMotionModule : MonoBehaviour
    {
        public const string NameID = "SNMotion";

        public const string FriendlyName = "Subnautica VR Motion Controls";

        public GameObject playerRig;
        private bool loaded = false;
      

        void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {

            GameObject camera = GameObject.Find("/EnvironmentCamera");

            if (camera != null && loaded == false) {
                Debug.Log("Attaching Controllers to camera parent: " + camera.transform.ToString());
                GameObject _prefab = SNMotion_AssetManager.assets.LoadAsset("SNMotionPlayer") as GameObject;
                playerRig = Instantiate(_prefab, camera.transform);
                SNMotion_Player player = playerRig.GetComponent<SNMotion_Player>();
                player.camera = camera.GetComponentInChildren<Camera>();
                player.SetUIMode(SNMotion_Player.UIModes.Menu);
                player.camera.cullingMask = -1;
                loaded = true;
            } else {
                Debug.Log("OnSceneLoaded: Did not load controllers for scene {0}: could not find camera");
            }
        }

        public void Patch()
        {
            VROptions.gazeBasedCursor = false;
            VROptions.aimRightArmWithHead = false;

            SceneManager.sceneLoaded += OnSceneLoaded;
            SteamVR_Settings.instance.actionsFilePath = "./QMods/SNMotion/actions.json";
            var harmony = HarmonyInstance.Create("com.github.cndw.snmotion");
            harmony.PatchAll(Assembly.GetExecutingAssembly());
         
        }
    }
}