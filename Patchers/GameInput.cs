﻿using System;
using CNDW.Harmony;
using UnityEngine;

namespace SNMotion.Patchers {
    [HarmonyPatch(typeof(GameInput))]
    [HarmonyPatch("UpdateAxisValues")]
    class GameInput_UpdateAxisValues_Patch {
        static float[] axisValues = Traverse.Create(typeof(GameInput)).Field("axisValues").GetValue<float[]>();
        [HarmonyPostfix]
        public static void Postfix() {
            SNMotion_Player player = SNMotion_Player.main;
            if (player != null) {
                Vector2 leftAxis = player._leftHand.move.GetAxis(player.leftHand.handType);
                Vector2 rightAxis = player._rightHand.move.GetAxis(player.rightHand.handType);
                axisValues[2] = leftAxis.x;
                axisValues[3] = -leftAxis.y;
                //axisValues[0] = rightAxis.x;
                //axisValues[1] = rightAxis.y;
            }
        }
    }


    [HarmonyPatch(typeof(GameInput))]
    [HarmonyPatch("GetButtonDown")]
    class GameInput_GetButtonDown_Patch {
        static GameInput.Button[] handledButtons = new GameInput.Button[] {
            GameInput.Button.LeftHand,
            GameInput.Button.RightHand
        };

        static bool GetButtonDownValue(GameInput.Button button) {
            switch (button) {
                case GameInput.Button.LeftHand:
                    return SNMotion_Player.main.GetHandTriggerDown("LeftHand");
                case GameInput.Button.RightHand:
                    return SNMotion_Player.main.GetHandTriggerDown("RightHand");
            }

            return false;
        }

        [HarmonyPrefix]
        public static bool Prefix(ref bool __result, GameInput.Button button) {
            if (Array.IndexOf(handledButtons, button) > -1) {
                __result = GetButtonDownValue(button);
                return false;
            }
            return true;
        }
    }


    [HarmonyPatch(typeof(GameInput))]
    [HarmonyPatch("GetButtonUp")]
    class GameInput_GetButtonUp_Patch {
        static GameInput.Button[] handledButtons = new GameInput.Button[] {
            GameInput.Button.LeftHand,
            GameInput.Button.RightHand
        };

        static bool GetButtonUpValue(GameInput.Button button) {
            switch (button) {
                case GameInput.Button.LeftHand:
                    return SNMotion_Player.main.GetHandTriggerUp("LeftHand");
                case GameInput.Button.RightHand:
                    return SNMotion_Player.main.GetHandTriggerUp("RightHand");
            }

            return false;
        }

        [HarmonyPrefix]
        public static bool Prefix(ref bool __result, GameInput.Button button) {
            if (Array.IndexOf(handledButtons, button) > -1) {
                __result = GetButtonUpValue(button);
                return false;
            }
            return true;
        }
    }


    [HarmonyPatch(typeof(GameInput))]
    [HarmonyPatch("GetButtonHeld")]
    class GameInput_GetButtonHeld_Patch {
        static GameInput.Button[] handledButtons = new GameInput.Button[] {
            GameInput.Button.LeftHand,
            GameInput.Button.RightHand
        };

        static bool GetButtonHeldValue(GameInput.Button button) {
            switch (button) {
                case GameInput.Button.LeftHand:
                    return SNMotion_Player.main.GetHandTriggerHeld("LeftHand");
                case GameInput.Button.RightHand:
                    return SNMotion_Player.main.GetHandTriggerHeld("RightHand");
            }

            return false;
        }

        [HarmonyPrefix]
        public static bool Prefix(ref bool __result, GameInput.Button button) {
            if (Array.IndexOf(handledButtons, button) > -1) {
                __result = GetButtonHeldValue(button);
                return false;
            }
            return true;
        }
    }
}
