﻿using CNDW.Harmony;
using UnityEngine;
using VRTK;

namespace SNMotion.Patchers {
    //[HarmonyPatch(typeof(uGUI_CraftingMenu))]
    //[HarmonyPatch("OnWillRenderCanvases")]
    //class uGUI_CraftingMenu_OnWillRenderCanvases_Patch {
    //    static void adjustLayers(GameObject obj) {
    //        obj.layer = 0;
    //        foreach (Transform child in obj.transform) {
    //            adjustLayers(child.gameObject);
    //        }
    //    }

    //    [HarmonyPrefix]
    //    public static bool Prefix(ref uGUI_CraftingMenu __instance, bool ___isDirty) {
    //        Debug.Log("CraftingMenu::OnWillRenderCanvases");
    //        if (___isDirty) {
    //            Debug.Log("CraftingMenu::OnWillRenderCanvases isDirty");

    //            __instance.OnWillRenderCanvases();

    //            adjustLayers(__instance.gameObject);
    //            VRTK_UICanvas existing = __instance.GetComponent<VRTK_UICanvas>();
    //            if (existing != null) {
    //                GameObject.Destroy(existing);
    //            }
    //            __instance.gameObject.AddComponent<VRTK_UICanvas>();
    //        }
    //        return false;
    //    }
    //}

    [HarmonyPatch(typeof(uGUI_CraftingMenu))]
    [HarmonyPatch("Open")]
    class uGUI_CraftingMenu_Open_Patch {
        [HarmonyPostfix]
        public static void Postfix(ref uGUI_CraftingMenu __instance) {

            RectTransform t = (RectTransform)__instance.transform;
            Transform cameraTransform = SNMotion_Player.main.camera.transform;
            Vector3 vector = cameraTransform.position + cameraTransform.forward * 0.75f;
            Quaternion quaternion = cameraTransform.rotation;

            t.SetPositionAndRotation(vector, quaternion);

            __instance.gameObject.AddComponent<VRTK_UICanvas>();

        }
    }


    [HarmonyPatch(typeof(uGUI_CraftingMenu))]
    [HarmonyPatch("Close")]
    class uGUI_CraftingMenu_Close_Patch {

        [HarmonyPostfix]
        public static void Postfix(ref uGUI_CraftingMenu __instance) {

            VRTK_UICanvas existing = __instance.GetComponent<VRTK_UICanvas>();
            if (existing != null) {
                GameObject.Destroy(existing);
            }
        }
    }
}
