﻿using CNDW.Harmony;
using UnityEngine;

namespace SNMotion.Patchers {
    //[HarmonyPatch(typeof(UnderwaterMotor))]
    //[HarmonyPatch("UpdateMove")]
    //class UnderwaterMotor_UpdateMove_Patch {
    //    static Traverse playerController = Traverse.Create(typeof(PlayerController)).Field("playerController").GetValue<>();
    //    // Adapted from SN source to ensure it works with the rest of the game
    //    // because of how it is architected, we must reworking the underwater movement
    //    // to decouple the movement direction from the camera
    //    public static Vector3 UpdateMove(ref UnderwaterMotor __instance) {
    //        Rigidbody component = __instance.GetComponent<Rigidbody>();

    //        if (__instance.playerController == null || __instance.playerController.forwardReference == null) {
    //            return component.velocity;
    //        }
    //        __instance.fastSwimMode = (Application.isEditor && Input.GetKey(KeyCode.LeftShift));
    //        Vector3 velocity = component.velocity;
    //        Vector3 vector = __instance.movementInputDirection;
    //        float y = vector.y;
    //        float num = Mathf.Min(1f, vector.magnitude);
    //        vector.y = 0f;
    //        vector.Normalize();
    //        float num2 = 0f;
    //        if (vector.z > 0f) {
    //            num2 = __instance.forwardMaxSpeed;
    //        } else if (vector.z < 0f) {
    //            num2 = -__instance.backwardMaxSpeed;
    //        }
    //        if (vector.x != 0f) {
    //            num2 = Mathf.Max(num2, __instance.strafeMaxSpeed);
    //        }
    //        num2 = Mathf.Max(num2, __instance.verticalMaxSpeed);
    //        num2 = __instance.AlterMaxSpeed(num2);
    //        bool flag = Player.main.GetBiomeString() == "wreck";
    //        bool flag2 = Player.main.motorMode == Player.MotorMode.Seaglide;
    //        if (flag && flag2) {
    //            num2 = __instance.seaglideWreckMaxSpeed;
    //        }
    //        num2 *= Player.main.mesmerizedSpeedMultiplier;
    //        if (__instance.fastSwimMode) {
    //            num2 *= 1000f;
    //        }
    //        num2 *= __instance.debugSpeedMult;
    //        float magnitude = velocity.magnitude;
    //        float num3 = Mathf.Max(magnitude, num2);
    //        Vector3 vector2 = __instance.playerController.forwardReference.rotation * vector;
    //        vector = vector2;
    //        vector.y += y;
    //        vector.Normalize();
    //        if (!__instance.canSwim) {
    //            vector.y = 0f;
    //            vector.Normalize();
    //        }
    //        float num4 = __instance.airAcceleration;
    //        if (__instance.grounded) {
    //            num4 = __instance.groundAcceleration;
    //        } else if (__instance.underWater) {
    //            num4 = __instance.acceleration;
    //            if (Player.main.GetBiomeString() == "wreck") {
    //                num4 *= 0.5f;
    //            } else if (Player.main.motorMode == Player.MotorMode.Seaglide) {
    //                num4 *= 1.45f;
    //            }
    //        }
    //        float num5 = num * num4 * Time.deltaTime;
    //        if (num5 > 0f) {
    //            Vector3 vector3 = velocity + vector * num5;
    //            if (vector3.magnitude > num3) {
    //                vector3.Normalize();
    //                vector3 *= num3;
    //            }
    //            float d = Vector3.Dot(vector3, __instance.surfaceNormal);
    //            if (!__instance.canSwim) {
    //                vector3 -= d * __instance.surfaceNormal;
    //            }
    //            bool flag3 = vector2.y > 0.6f;
    //            bool flag4 = vector2.y < -0.3f;
    //            bool flag5 = y < 0f;
    //            if (__instance.transform.position.y >= 0.6f && !flag3 && !flag4 && !flag5) {
    //                vector3.y = 0f;
    //            }
    //            component.velocity = vector3;
    //            __instance.desiredVelocity = vector3;
    //        } else {
    //            __instance.desiredVelocity = component.velocity;
    //        }
    //        float num6 = (!__instance.underWater) ? __instance.gravity : __instance.underWaterGravity;
    //        if (num6 != 0f) {
    //            component.AddForce(new Vector3(0f, -num6 * Time.deltaTime, 0f), ForceMode.VelocityChange);
    //            __instance.usingGravity = true;
    //        } else {
    //            __instance.usingGravity = false;
    //        }
    //        float drag = __instance.airDrag;
    //        if (__instance.underWater) {
    //            drag = __instance.swimDrag;
    //        } else if (__instance.grounded) {
    //            drag = __instance.groundDrag;
    //        }
    //        component.drag = drag;
    //        InertiaGene component2 = __instance.gameObject.GetComponent<InertiaGene>();
    //        if (component2) {
    //            component.drag -= component2.Scalar * component.drag;
    //        }
    //        if (__instance.fastSwimMode) {
    //            component.drag = 0f;
    //        }
    //        __instance.grounded = false;
    //        __instance.vel = component.velocity;
    //        return __instance.vel;
    //    }

    //    [HarmonyPrefix]
    //    public static bool Prefix(ref UnderwaterMotor __instance, ref Vector3 __result) {
    //        return false;
    //    }
    //}
}
