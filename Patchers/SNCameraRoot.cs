﻿using CNDW.Harmony;
using UnityEngine;

namespace SNMotion.Patchers {
    [HarmonyPatch(typeof(SNCameraRoot))]
    [HarmonyPatch("Start")]
    public class SNCameraRoot_Start_Patch : MonoBehaviour {
        [HarmonyPostfix]
        public static void Postfix(ref SNCameraRoot __instance) {

            GameObject _prefab = SNMotion_AssetManager.assets.LoadAsset("SNMotionPlayer") as GameObject;
            GameObject playerRig = Instantiate(_prefab, __instance.gameObject.transform);
            playerRig.name = "VRPlayerRig";

            SNMotion_Player player = playerRig.GetComponent<SNMotion_Player>();
            player.camera = __instance.mainCam;

            player.SetUIMode(SNMotion_Player.UIModes.FirstPerson);
        }
    }

    [HarmonyPatch(typeof(SNCameraRoot))]
    [HarmonyPatch("UpdateVR")]
    public class SNCameraRoot_UpdateVR_Patch {
        [HarmonyPrefix]
        public static bool Prefix(ref SNCameraRoot __instance) {
            return true;
        }
    }
}
