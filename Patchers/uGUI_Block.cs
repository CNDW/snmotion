﻿using CNDW.Harmony;
using UnityEngine;
using VRTK;

namespace SNMotion.Patchers {
    [HarmonyPatch(typeof(uGUI_Block))]
    [HarmonyPatch("OnEnable")]
    class uGUI_Block_OnEnable_Patch {
        static void adjustLayers(GameObject obj) {
            obj.layer = 0;
            foreach (Transform child in obj.transform) {
                adjustLayers(child.gameObject);
            }
        }

        [HarmonyPrefix]
        public static bool Prefix(ref uGUI_Block __instance) {
            adjustLayers(__instance.gameObject);
            return true;
        }
    }
}
