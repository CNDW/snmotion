﻿using CNDW.Harmony;
using UnityEngine;
using VRTK;

namespace SNMotion.Patchers {
    [HarmonyPatch(typeof(uGUI_SafeAreaScaler))]
    [HarmonyPatch("Update")]
    class uGUI_SafeAreaScaler_Update_Patch {

        [HarmonyPrefix]
        public static bool Prefix(ref uGUI_SafeAreaScaler __instance) {
            return false;
        }
    }
}
