﻿using CNDW.Harmony;
using UnityEngine;
using VRTK;

namespace SNMotion.Patchers {
    [HarmonyPatch(typeof(uGUI))]
    [HarmonyPatch("Awake")]
    class uGUI_Initialize_Patch {
        static void adjustLayers(GameObject obj) {
            obj.layer = 0;
            foreach (Transform child in obj.transform) {
                adjustLayers(child.gameObject);
            }
        }

        [HarmonyPostfix]
        public static void Postfix(ref uGUI __instance) {
            //adjustLayers(__instance.gameObject);
            Transform crafting = __instance.transform.Find("CraftingCanvas");
            if (crafting != null) {
                adjustLayers(crafting.gameObject);
            }
        }
    }
}
