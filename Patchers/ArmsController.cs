﻿using CNDW.Harmony;
using UnityEngine;
using VRTK;

namespace SNMotion.Patchers {
    [HarmonyPatch(typeof(ArmsController))]
    [HarmonyPatch("Start")]
    class HideForScreenshots_Start_Patch {

        [HarmonyPostfix]
        public static void Postfix(ref ArmsController __instance) {
            __instance.gameObject.SetActive(false);
        }
    }
}
