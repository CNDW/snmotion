﻿using CNDW.Harmony;
using UnityEngine;

namespace SNMotion.Patchers {

    [HarmonyPatch(typeof(VRInitialization))]
    [HarmonyPatch("InitializeSteamVR")]
    public class VRInitialization_InitializeSteamVR_Patch : MonoBehaviour
    {

        [HarmonyPrefix]
        public static bool Prefix(ref VRInitialization __instance)
        {
            Debug.Log("Aborting subnautica VRInitialization...");

            return false;
        }
    }
}