﻿using CNDW.Harmony;
using UnityEngine;
using VRTK;

namespace SNMotion.Patchers {
    [HarmonyPatch(typeof(uGUI_CanvasScaler))]
    [HarmonyPatch("UpdateTransform")]
    class uGUI_CanvasScaler_UpdateTransform_Patch {

        [HarmonyPrefix]
        public static bool Prefix(ref uGUI_CanvasScaler __instance) {
            uGUI_MainMenu menu = __instance.GetComponent<uGUI_MainMenu>();
            uGUI_CraftingMenu cMenu = __instance.GetComponent<uGUI_CraftingMenu>();
            return menu == null && cMenu == null;
        }
    }
    
    [HarmonyPatch(typeof(uGUI_CanvasScaler))]
    [HarmonyPatch("UpdateFrustum")]
    class uGUI_CanvasScaler_UpdateFrustum_Patch {

        [HarmonyPrefix]
        public static bool Prefix(ref uGUI_CanvasScaler __instance) {
            uGUI_MainMenu menu = __instance.GetComponent<uGUI_MainMenu>();
            return menu == null;
        }
    }
}
