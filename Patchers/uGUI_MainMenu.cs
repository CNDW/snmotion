﻿using CNDW.Harmony;
using UnityEngine;
using VRTK;

namespace SNMotion.Patchers {
    [HarmonyPatch(typeof(uGUI_MainMenu))]
    [HarmonyPatch("Start")]
    public class uGUI_MainMenu_Start_Patch {
        public static float baseScale = 0.01587302f;
        [HarmonyPostfix]
        public static void Postfix(ref uGUI_MainMenu __instance) {
            // Resize the menu for vr
            GameObject mainMenu = __instance.gameObject.GetComponentInChildren<MainMenuPrimaryOptionsMenu>().gameObject;

            RectTransform t = (RectTransform)__instance.transform;
            t.localScale = new Vector3(0.0015f, 0.0015f, 1f);
            t.anchoredPosition3D = new Vector3(0, 4f, 1f);
            t.forward = new Vector3(0f, 0f, 1f);
            Canvas canvas = __instance.GetComponent<Canvas>();
            canvas.scaleFactor = 1f;

            VRTK_UICanvas uiCanvas = __instance.gameObject.AddComponent<VRTK_UICanvas>();
            //uiCanvas.autoActivateWithinDistance = 0.2f;
            //uiCanvas.CreateActivator(canvas);
        }
    }

}
