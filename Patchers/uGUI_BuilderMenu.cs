﻿using CNDW.Harmony;
using UnityEngine;
using VRTK;

namespace SNMotion.Patchers {
    [HarmonyPatch(typeof(uGUI_BuilderMenu))]
    [HarmonyPatch("UpdateItems")]
    class uGUI_BuilderMenu_UpdateItems_Patch {
        static void adjustLayers(GameObject obj) {
            obj.layer = 0;
            foreach (Transform child in obj.transform) {
                adjustLayers(child.gameObject);
            }
        }

        [HarmonyPostfix]
        public static void Postfix(ref uGUI_BuilderMenu __instance) {
            adjustLayers(__instance.gameObject);

            Canvas canvas = __instance.GetComponent<Canvas>();

            VRTK_UICanvas uiCanvas = __instance.gameObject.AddComponent<VRTK_UICanvas>();
        }
    }
}
