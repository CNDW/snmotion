﻿using CNDW.Harmony;
using UnityEngine;


namespace SNMotion.Patchers {
    [HarmonyPatch(typeof(MainCameraControl))]
    [HarmonyPatch("Awake")]
    class MainCameraControl_Awake_Patch {
        [HarmonyPostfix]
        public static void Postfix(ref MainCameraControl __instance) {
            Transform camOffset = __instance.transform.Find("camOffset");
            camOffset.localPosition = new Vector3(0f, -2.7f, 0f);
        }
    }
}
