using System;
using Common;
using ModInspector;

namespace SNMotion.Patchers
{

    public class QPatch
    {
        public static void Patch()
        {
            try
            {
                QuickLogger.Message("Applying Inspector instrumentation");


                HierarchyLogger.Initialize();

                QuickLogger.Message("Start patching. Version: " + QuickLogger.GetAssemblyVersion());
                var snMotion = new SNMotionModule();

                snMotion.Patch();
                QuickLogger.Message("Finished patching");
            }
            catch (Exception ex)
            {
                QuickLogger.Error("EXCEPTION on Patch: " + ex.ToString());
            }
        }
    }
}